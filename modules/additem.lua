-- [[ additem dialog ]] --
local function AnimateBackdrop()
  local r, g, b = this:GetBackdropBorderColor()
  if r <= .2 and g <= .2 and b <= .2 then
    return
  else
    local r = r > .2 and r - .02 or r
    local g = g > .2 and g - .02 or g
    local b = b > .2 and b - .02 or b
    this:SetBackdropBorderColor(r, g, b)
  end
end

local function CreateAddItemDialog()
  local f = CreateFrame("Frame", "AddItemDialog", UIParent)
  f:Hide()
  f:SetPoint("CENTER", 0, 0)
  f:SetHeight(200)
  f:SetWidth(320)
  f:SetScript("OnShow", function()
    pfAdmin.AddItemDialog:ClearFields()
  end)

  pfUI.api.CreateBackdrop(f, default_border)

  f.title = f:CreateFontString("Status", "LOW", "GameFontNormal")
  f.title:SetFontObject(GameFontWhite)
  f.title:SetPoint("TOP", f, "TOP", 0, -8)
  f.title:SetJustifyH("LEFT")
  f.title:SetFont(pfUI.font_default, 14)
  f.title:SetText("|cff33ffccpf|rAdmin: Create Items")

  do -- row: player name
    f.itemcountText = f:CreateFontString("Status", "LOW", "GameFontNormal")
    f.itemcountText:SetFontObject(GameFontWhite)
    f.itemcountText:SetPoint("TOPLEFT", 10, -40)
    f.itemcountText:SetWidth(150)
    f.itemcountText:SetHeight(30)
    f.itemcountText:SetJustifyH("LEFT")
    f.itemcountText:SetFont(pfUI.font_default, 14)
    f.itemcountText:SetText("Item Count:")

    f.countInput = CreateFrame("EditBox", "SendItemDialogPlayerName", f)
    pfUI.api.CreateBackdrop(f.countInput, nil, true)
    f.countInput:SetFont(pfUI.font_default, pfUI_config.global.font_size, "OUTLINE")
    f.countInput:SetAutoFocus(false)
    f.countInput:SetJustifyH("RIGHT")
    f.countInput:SetPoint("TOPRIGHT", -10, -40)
    f.countInput:SetWidth(150)
    f.countInput:SetHeight(30)
    f.countInput:SetTextInsets(10,10,5,5)
    f.countInput:SetScript("OnEscapePressed", function() this:ClearFocus() end)
    f.countInput:SetScript("OnTextChanged", function()
      this:SetBackdropBorderColor(.3, 1, .8)
    end)
    f.countInput:SetScript("OnUpdate", function() AnimateBackdrop() end)
  end

  do -- row: item id
    f.itemidText = f:CreateFontString("Status", "LOW", "GameFontNormal")
    f.itemidText:SetFontObject(GameFontWhite)
    f.itemidText:SetPoint("TOPLEFT", 10, -80)
    f.itemidText:SetWidth(150)
    f.itemidText:SetHeight(30)
    f.itemidText:SetJustifyH("LEFT")
    f.itemidText:SetFont(pfUI.font_default, 14)
    f.itemidText:SetText("Item ID:")

    f.itemidInput = CreateFrame("EditBox", "SendItemDialogItemID", f)
    pfUI.api.CreateBackdrop(f.itemidInput, nil, true)
    f.itemidInput:SetFont(pfUI.font_default, pfUI_config.global.font_size, "OUTLINE")
    f.itemidInput:SetAutoFocus(false)
    f.itemidInput:SetJustifyH("RIGHT")
    f.itemidInput:SetPoint("TOPRIGHT", -10, -80)
    f.itemidInput:SetWidth(150)
    f.itemidInput:SetHeight(30)
    f.itemidInput:SetTextInsets(10,10,5,5)
    f.itemidInput:SetScript("OnEscapePressed", function() this:ClearFocus() end)
    f.itemidInput:SetScript("OnTextChanged", function()
      this:SetBackdropBorderColor(.3, 1, .8)
      local text = this:GetText()
      if text ~= "0" then
      else
        -- clear item name
      end
    end)

    f.itemidInput:SetScript("OnEnter", function()
      local id = tonumber(this:GetText()) or 0
      local info = GetItemInfo(id)
      if info then
        GameTooltip:SetOwner(this, "ANCHOR_RIGHT", -10, -5)
        GameTooltip:SetHyperlink("item:" .. this:GetText() .. ":0:0:0")
        GameTooltip:Show()
      end
    end)

    f.itemidInput:SetScript("OnLeave", function()
      GameTooltip:Hide()
    end)

    f.itemidInput:SetScript("OnUpdate", function() AnimateBackdrop() end)
  end

  do -- button: close
    f.closeButton = CreateFrame("Button", "AddItemDialogClose", f)
    f.closeButton:SetPoint("TOPRIGHT", -5, -5)
    f.closeButton:SetHeight(12)
    f.closeButton:SetWidth(12)
    f.closeButton.texture = f.closeButton:CreateTexture("pfQuestionDialogCloseTex")
    f.closeButton.texture:SetTexture("Interface\\AddOns\\pfQuest\\compat\\close")
    f.closeButton.texture:ClearAllPoints()
    f.closeButton.texture:SetAllPoints(f.closeButton)
    f.closeButton.texture:SetVertexColor(1,.25,.25,1)
    pfUI.api.SkinButton(f.closeButton, 1, .5, .5)
    f.closeButton:SetScript("OnClick", function()
     this:GetParent():Hide()
    end)
  end

  do -- button: abort
    f.abortButton = CreateFrame("Button", "AddItemDialogAbort", f)
    pfUI.api.SkinButton(f.abortButton)
    f.abortButton:SetPoint("BOTTOMLEFT", 10, 10)
    f.abortButton:SetWidth(100)
    f.abortButton:SetHeight(30)
    f.abortButton.text = f.abortButton:CreateFontString("Caption", "LOW", "GameFontWhite")
    f.abortButton.text:SetAllPoints(f.abortButton)
    f.abortButton.text:SetFont(pfUI.font_default, pfUI_config.global.font_size, "OUTLINE")
    f.abortButton.text:SetText("Abort")
    f.abortButton:SetScript("OnClick", function()
      this:GetParent():Hide()
    end)
  end

  do -- button: send
    f.sendButton = CreateFrame("Button", "AddItemDialogSend", f)
    pfUI.api.SkinButton(f.sendButton)
    f.sendButton:SetPoint("BOTTOMRIGHT", -10, 10)
    f.sendButton:SetWidth(100)
    f.sendButton:SetHeight(30)
    f.sendButton.text = f.sendButton:CreateFontString("Caption", "LOW", "GameFontWhite")
    f.sendButton.text:SetAllPoints(f.sendButton)
    f.sendButton.text:SetFont(pfUI.font_default, pfUI_config.global.font_size, "OUTLINE")
    f.sendButton.text:SetText("Create")
    f.sendButton:SetScript("OnClick", function()
      local parent = this:GetParent()
      local itemid = parent.itemidInput:GetText()
      local itemcount = parent.countInput:GetText()

      local command = pfAdmin.commands[pfAdmin_config["server"]]["ADDITEM"][1]
      command = string.gsub(command, "#ITEMID", itemid)
      command = string.gsub(command, "#COUNT", itemcount)
      
      SendChatMessage(command)

    end)
  end

  return f
end

pfAdmin.AddItemDialog = CreateAddItemDialog()
pfAdmin.AddItemDialog:SetFrameStrata("FULLSCREEN_DIALOG")
pfAdmin.AddItemDialog:SetMovable(true)
pfAdmin.AddItemDialog:EnableMouse(true)
pfAdmin.AddItemDialog:SetScript("OnMouseDown",function()
  this:StartMoving()
end)

pfAdmin.AddItemDialog:SetScript("OnMouseUp",function()
  this:StopMovingOrSizing()
end)

function pfAdmin.AddItemDialog:AddItem(link)
  pfAdmin.AddItemDialog:Show()
  local _, _, itemid = string.find(link, "item:(%d+):%d+:%d+:%d+")
  pfAdmin.AddItemDialog.itemidInput:SetText(itemid)
end

function pfAdmin.AddItemDialog:ClearFields()
  pfAdmin.AddItemDialog.countInput:SetText("1")
  pfAdmin.AddItemDialog.itemidInput:SetText("0")
end

-- [[ hook functions ]] --
local HookSetItemRef = SetItemRef
function SetItemRef(link, text, button)
  if button == "RightButton" then
    -- fill in item id
    if strsub(link, 1, 4) == "item" and not ChatFrameEditBox:IsVisible() then
      pfAdmin.AddItemDialog:AddItem(link)
      return
    end
  end

  HookSetItemRef(link, text, button)
end
